-- Host: localhost    Database: store_JDBC
-- ------------------------------------------------------
-- Server version	5.7.17-log

CREATE DATABASE IF NOT EXISTS store_JDBC;
USE store_JDBC;
CREATE TABLE IF NOT EXISTS items (
                                     ID            int         AUTO_INCREMENT PRIMARY KEY,
                                     name          varchar(20) NOT NULL,
                                     price         decimal     NULL,
                                     weight        float       NOT NULL,
                                     creation_date datetime    NULL,
                                     color         varchar(10) NULL
);

CREATE TABLE IF NOT EXISTS clients (
                                       ID            int         AUTO_INCREMENT PRIMARY KEY,
                                       name          varchar(20) NOT NULL,
                                       surname       varchar(30) NOT NULL,
                                       email         varchar(50) NULL,
                                       discount_card  int        NULL,
                                       wish_list 	   int		   NULL,
                                       phone_number  varchar(11) NULL,
                                       money decimal default 0
);
CREATE TABLE IF NOT EXISTS discount_cards (
                                              ID            int         AUTO_INCREMENT PRIMARY KEY,
                                              name          varchar(20) NOT NULL,
                                              percent       int         NOT NULL,
                                              client_id     int         NOT NULL
);
ALTER TABLE discount_cards
    ADD CONSTRAINT id_client_cards
        FOREIGN KEY (client_id)
            REFERENCES clients(ID);

ALTER TABLE clients
    ADD CONSTRAINT card_discount_clients
        FOREIGN KEY (discount_card)
            REFERENCES discount_cards(ID)
            ON DELETE CASCADE ON UPDATE SET NULL
;
CREATE TABLE IF NOT EXISTS wishes_list (
                                           ID 	   int 	AUTO_INCREMENT PRIMARY KEY,
                                           item_id   int  NOT NULL,
                                           client_id int 	NOT NULL,
                                           FOREIGN KEY (client_id) REFERENCES clients(ID)
);
ALTER TABLE wishes_list
    ADD CONSTRAINT id_item
        FOREIGN KEY (item_id)
            REFERENCES items(ID);


CREATE TABLE IF NOT EXISTS basket (
                                      ID int AUTO_INCREMENT PRIMARY KEY,
                                      item_id int NOT NULL,
                                      ammount int DEFAULT 1,
                                      cost decimal NOT NULL,
                                      client_id int NOT NULL
);

ALTER TABLE basket
    ADD CONSTRAINT id_item_basket
        FOREIGN KEY (item_id)
            REFERENCES items(ID);

ALTER TABLE basket
    ADD CONSTRAINT id_client_basket
        FOREIGN KEY (client_id)
            REFERENCES clients(ID);


CREATE TABLE IF NOT EXISTS orders (
                                      ID int AUTO_INCREMENT PRIMARY KEY,
                                      basket_id int NOT NULL,
                                      price decimal NOT NULL,
                                      client_id int NOT NULL
);

ALTER TABLE orders
    ADD CONSTRAINT id_basket_orders
        FOREIGN KEY (basket_id)
            REFERENCES basket(ID);

ALTER TABLE orders
    ADD CONSTRAINT id_client_orders
        FOREIGN KEY (client_id)
            REFERENCES clients(ID);

CREATE TABLE IF NOT EXISTS ship (
                                    ID int AUTO_INCREMENT PRIMARY KEY,
                                    order_id int NOT NULL,
                                    ship_cost decimal DEFAULT 50,
                                    from_adress varchar (50) DEFAULT "Lviv",
                                    to_adress varchar (50) NOT NULL,
                                    post_service enum ("Ukrposta","Novaposta") DEFAULT "Novaposta"
);
ALTER TABLE ship
    ADD CONSTRAINT order_id_ship
        FOREIGN KEY (order_id)
            REFERENCES orders(ID);


-- Dump completed on 2017-12-15 21:31:57
