package com.kozlov.ViewAndController;

import com.kozlov.model.*;
import com.kozlov.model.metadata.TableMetaData;
import com.kozlov.persistant.ConnectionManager;
import com.kozlov.service.*;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.*;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("A", "   A - Select all table");
        menu.put("B", "   B - Select structure of DB");


        menu.put("2", "   2 - Table: Clients");
        menu.put("21", "  21 - Create for Employee");
        menu.put("22", "  22 - Update Employee");
        menu.put("23", "  23 - Delete from Employee");
        menu.put("24", "  24 - Select Employee");
        menu.put("25", "  25 - Find Employee by ID");
        menu.put("26", "  26 - Find Employee by Name");

        menu.put("Q", "   Q - exit");

        methodsMenu.put("A", this::selectAllTable);
        methodsMenu.put("B", this::takeStructureOfDB);

        methodsMenu.put("21", this::createForEmployee);
        methodsMenu.put("22", this::updateEmployee);
        methodsMenu.put("23", this::deleteFromEmployee);
        methodsMenu.put("24", this::selectClient);
        methodsMenu.put("25", this::findEmployeeByID);
        methodsMenu.put("26", this::findEmployeeByName);

    }

    private void selectAllTable() throws SQLException {
        selectClient();
    }

    private void takeStructureOfDB() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        MetaDataService metaDataService = new MetaDataService();
        List<TableMetaData> tables = metaDataService.getTablesStructure();
        System.out.println("TABLE OF DATABASE: "+connection.getCatalog());

        for (TableMetaData table: tables ) {
            System.out.println(table);
        }
    }


    //------------------------------------------------------------------------

    private void deleteFromEmployee() throws SQLException {
        System.out.println("Input ID(dept_no) for Employee: ");
        String id = input.nextLine();
        input.nextLine();
        ClientsService clientsService = new ClientsService();
        int count = clientsService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForEmployee() throws SQLException {
        System.out.println("Input ID(epm_no) for Client: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.println("Input emp_fmane for Client: ");
        String name = input.nextLine();
        System.out.println("Input emp_lname for Client: ");
        String surname = input.nextLine();
        System.out.println("Input dept_no for Client: ");
        String deptNo = input.nextLine();
        System.out.println("Input email for Client: ");
        String email = input.nextLine();
        System.out.println("Input discountcard for Client: ");
        Integer discountCard = input.nextInt();
        System.out.println("Input wishlist for Client: ");
        Integer wishList = input.nextInt();
        System.out.println("Input poneNumber for Client: ");
        String phoneNumber = input.nextLine();
        System.out.println("Input money for Client: ");
        BigDecimal money = input.nextBigDecimal();
        ClientsEntity entity = new ClientsEntity(id, name, surname, email, discountCard, wishList,phoneNumber,
        money);
        ClientsService clientsService = new ClientsService();

        int count = clientsService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateEmployee() throws SQLException {
        System.out.println("Input ID(epm_no) for Client: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.println("Input emp_fmane for Client: ");
        String name = input.nextLine();
        System.out.println("Input emp_lname for Client: ");
        String surname = input.nextLine();
        System.out.println("Input dept_no for Client: ");
        String deptNo = input.nextLine();
        System.out.println("Input email for Client: ");
        String email = input.nextLine();
        System.out.println("Input discountcard for Client: ");
        Integer discountCard = input.nextInt();
        System.out.println("Input wishlist for Client: ");
        Integer wishList = input.nextInt();
        System.out.println("Input poneNumber for Client: ");
        String phoneNumber = input.nextLine();
        System.out.println("Input money for Client: ");
        BigDecimal money = input.nextBigDecimal();
        ClientsEntity entity = new ClientsEntity(id, name, surname, email, discountCard, wishList,phoneNumber,
                money);
        ClientsService clientsService = new ClientsService();

        int count = clientsService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void selectClient() throws SQLException {
        System.out.println("\nTable: Clients");
        ClientsService clientsService = new ClientsService();
        List<ClientsEntity> employees = clientsService.findAll();
        for (ClientsEntity entity : employees) {
            System.out.println(entity);
        }
    }

    private void findEmployeeByID() throws SQLException {
        System.out.println("Input ID(emp_no) for Employee: ");
        String id = input.nextLine();
        input.nextLine();
        ClientsService clientsService = new ClientsService();
        ClientsEntity entity = clientsService.findById(id);
        System.out.println(entity);
    }

    private void findEmployeeByName() throws SQLException {
        System.out.println("Input First Name for Employee: ");
        String fname = input.nextLine();
        ClientsService clientsService = new ClientsService();
        ClientsEntity employees = clientsService.findByName(fname);
            System.out.println(employees);
    }

    //------------------------------------------------------------------------



    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet())
            if (key.length() == 1) System.out.println(menu.get(key));
    }

    private void outputSubMenu(String fig) {

        System.out.println("\nSubMENU:");
        for (String key : menu.keySet())
            if (key.length() != 1 && key.substring(0, 1).equals(fig)) System.out.println(menu.get(key));
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();

            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                System.out.println("Please, select menu point.");
                keyMenu = input.nextLine().toUpperCase();
            }

            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
