package com.kozlov.model;


import com.kozlov.model.Annotation.Column;
import com.kozlov.model.Annotation.PrimaryKey;
import com.kozlov.model.Annotation.Table;

import java.math.BigDecimal;

@Table(name = "ClientsEntity")
public class ClientsEntity {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "name", length = 20)
    private String name;
    @Column(name = "surname", length = 30)
    private String surname;
    @Column(name = "email", length = 50)
    private String email;
    @Column(name = "discount_card")
    private int discountCard;
    @Column(name = "wish_list")
    private int wishList;
    @Column (name = "phone_number")
    private String phoneNumber;
    @Column(name = "money")
    private BigDecimal money = new BigDecimal(0);
    public ClientsEntity(int id, String name, String surname, String email, int discountCard, int wishList, String phoneNumber, BigDecimal money){
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.discountCard = discountCard;
        this.wishList = wishList;
        this.phoneNumber = phoneNumber;
        this.money = money;
    }
    ClientsEntity(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getDiscountCard() {
        return discountCard;
    }

    public void setDiscountCard(int discountCard) {
        this.discountCard = discountCard;
    }

    public int getWishList() {
        return wishList;
    }

    public void setWishList(int wishList) {
        this.wishList = wishList;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
    public String toString() {
        return (id + " " + name + " " + surname + " " + email + " " +  discountCard + " " +  wishList + " " +  phoneNumber + " " +  money);
    }
}
