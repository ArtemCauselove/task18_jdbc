package com.kozlov.model;

import com.kozlov.model.Annotation.Column;
import com.kozlov.model.Annotation.PrimaryKey;
import com.kozlov.model.Annotation.Table;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Table(name = "ItemsEntity")
public class ItemsEntity {
    @PrimaryKey
    @Column(name ="id" )
    private int id;
    @Column(name ="name" )
    private String name;
    @Column(name ="price" )
    private BigDecimal price = new BigDecimal(0);
    @Column(name ="weight" )
    private float weight;
    @Column(name ="creation_date" )
    private java.sql.Date creationDate;
    @Column(name ="color" )
    private String color;

    public ItemsEntity(int id, String name, float weight, java.sql.Date creationDate, String color) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.creationDate = creationDate;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public java.sql.Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(java.sql.Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public String toString() {
        return (id + " " + name + " " + price + " " + weight + " " +  creationDate + " " +  color);
    }
}
