package com.kozlov.DAO;

import com.kozlov.model.ClientsEntity;

import java.sql.SQLException;

public interface ClientsDAO extends GeneralDAO<ClientsEntity, String> {
    ClientsEntity findByName(String name) throws SQLException;

    ClientsEntity findByEmail(String email) throws SQLException;
}
