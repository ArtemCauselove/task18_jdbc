package com.kozlov.DAO;

import com.kozlov.model.ItemsEntity;

import java.sql.SQLException;

public interface ItemsDAO extends GeneralDAO<ItemsEntity, String> {
    ItemsEntity findByName(String name) throws SQLException;

    ItemsEntity findByPrice(String price) throws SQLException;
}
