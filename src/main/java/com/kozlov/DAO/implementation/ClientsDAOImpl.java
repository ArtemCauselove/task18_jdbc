package com.kozlov.DAO.implementation;

import com.kozlov.DAO.ClientsDAO;
import com.kozlov.model.ClientsEntity;
import com.kozlov.persistant.ConnectionManager;
import com.kozlov.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientsDAOImpl implements ClientsDAO {
    private static final String FIND_ALL = "SELECT * FROM clients";
    private static final String DELETE = "DELETE FROM clients WHERE id=?";
    private static final String CREATE = "INSERT clients (id, name, surname, email, discount_cards, wish_list, phone_number, money) VALUES (?, ?, ?,?, ?, ?,?,?)";
    private static final String UPDATE = "UPDATE clients SET name=?, email=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM clients WHERE id=?";
    private static final String FIND_BY_EMAIL = "SELECT * FROM clients WHERE email=?";
    private static final String FIND_BY_NAME = "SELECT * FROM clients WHERE name=?";


    @Override
    public List<ClientsEntity> findAll() throws SQLException {
        List<ClientsEntity> clients = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    clients.add((ClientsEntity) new Transformer(ClientsEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return clients;
    }

    @Override
    public ClientsEntity findById(String id) throws SQLException {
        ClientsEntity entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (ClientsEntity) new Transformer(ClientsEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(ClientsEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getName());
            ps.setString(3, entity.getSurname());
            ps.setString(4, entity.getEmail());
            ps.setInt(4, entity.getDiscountCard());
            ps.setInt(5, entity.getWishList());
            ps.setString(6, entity.getPhoneNumber());
            ps.setBigDecimal(7, entity.getMoney());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(ClientsEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getName());
            ps.setString(2, entity.getEmail());
            ps.setInt(3, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(String id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public ClientsEntity findByName(String name) throws SQLException {
        ClientsEntity entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (ClientsEntity) new Transformer(ClientsEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public ClientsEntity findByEmail(String email) throws SQLException {
        ClientsEntity entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_EMAIL)) {
            ps.setString(1, email);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (ClientsEntity) new Transformer(ClientsEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

}
