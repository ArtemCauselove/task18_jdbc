package com.kozlov.DAO.implementation;

import com.kozlov.DAO.ItemsDAO;
import com.kozlov.model.ItemsEntity;
import com.kozlov.persistant.ConnectionManager;
import com.kozlov.transformer.Transformer;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ItemsDAOImpl implements ItemsDAO {
    private static final String FIND_ALL = "SELECT * FROM items";
    private static final String DELETE = "DELETE FROM items WHERE id=?";
    private static final String CREATE = "INSERT items (id, name, price, weight, creation_date, color) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE items SET name=?, price=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM items WHERE id=?";
    private static final String FIND_BY_PRICE = "SELECT * FROM items WHERE price=?";
    private static final String FIND_BY_NAME = "SELECT * FROM items WHERE name=?";


    @Override
    public List<ItemsEntity> findAll() throws SQLException {
        List<ItemsEntity> items = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    items.add((ItemsEntity) new Transformer(ItemsEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return items;
    }

    @Override
    public ItemsEntity findById(String id) throws SQLException {
        ItemsEntity entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (ItemsEntity) new Transformer(ItemsEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(ItemsEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getName());
            ps.setBigDecimal(3, entity.getPrice());
            ps.setFloat(4, entity.getWeight());
            ps.setDate(5, entity.getCreationDate());
            ps.setString(6, entity.getColor());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(ItemsEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getName());
            ps.setBigDecimal(2, entity.getPrice());
            ps.setInt(3, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(String id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public ItemsEntity findByPrice(String price) throws SQLException {
        ItemsEntity entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_PRICE)) {
            ps.setString(1, price);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (ItemsEntity) new Transformer(ItemsEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public ItemsEntity findByName(String name) throws SQLException {
        ItemsEntity entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (ItemsEntity) new Transformer(ItemsEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

}
