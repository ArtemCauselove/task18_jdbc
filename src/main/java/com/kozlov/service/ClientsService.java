package com.kozlov.service;
import com.kozlov.DAO.implementation.ClientsDAOImpl;
import com.kozlov.model.ClientsEntity;
import java.sql.SQLException;
import java.util.List;

public class ClientsService {
    public List<ClientsEntity> findAll() throws SQLException {
        return new ClientsDAOImpl().findAll();
    }

    public ClientsEntity findById(String id) throws SQLException {
        return new ClientsDAOImpl().findById(id);
    }

    public int create(ClientsEntity entity) throws SQLException {
        return new ClientsDAOImpl().create(entity);
    }

    public int update(ClientsEntity entity) throws SQLException {
        return new ClientsDAOImpl().update(entity);
    }

    public int delete(String id) throws SQLException {
        return new ClientsDAOImpl().delete(id);
    }

    public ClientsEntity findByName(String name) throws SQLException {
        return new ClientsDAOImpl().findByName(name);
    }
}
